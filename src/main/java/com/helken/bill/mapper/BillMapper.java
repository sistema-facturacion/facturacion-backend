package com.helken.bill.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.helken.bill.models.dao.BillDAO;
import com.helken.bill.models.dto.BillDTO;

@Mapper
public interface BillMapper {

    BillDTO toDto(BillDAO bill);

    BillDAO DtoTo(BillDTO bill);

    List<BillDTO> toDtos(List<BillDAO> bills);

    List<BillDAO> DtosTo(List<BillDTO> bills);
}