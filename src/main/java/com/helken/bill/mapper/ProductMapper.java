package com.helken.bill.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import com.helken.bill.models.dao.ProductDAO;
import com.helken.bill.models.dto.ProductDTO;

@Mapper
public interface ProductMapper {

    ProductDTO toDto(ProductDAO product);

    @InheritInverseConfiguration
    ProductDAO DtoTo(ProductDTO product);

    List<ProductDTO> toDtos(List<ProductDAO> products);

    @InheritInverseConfiguration
    List<ProductDAO> DtosTo(List<ProductDTO> products);
}