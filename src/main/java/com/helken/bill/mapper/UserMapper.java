package com.helken.bill.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import com.helken.bill.models.dao.UserDAO;
import com.helken.bill.models.dto.UserDTO;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = RoleMapper.class)
public interface UserMapper {

    UserDTO toDto(UserDAO user);

    @Mapping(target = "password", ignore = true)
    UserDAO DtoTo(UserDTO user);

    List<UserDTO> toDtos(List<UserDAO> users);

    @InheritInverseConfiguration
    List<UserDAO> DtosTo(List<UserDTO> users);
}