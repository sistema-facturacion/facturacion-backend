package com.helken.bill.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.helken.bill.models.dao.ClientDAO;
import com.helken.bill.models.dto.ClientDTO;

@Mapper
public interface ClientMapper {

    ClientDTO toDto(ClientDAO client);

    ClientDAO DtoTo(ClientDTO client);

    List<ClientDTO> toDtos(List<ClientDAO> clients);

    List<ClientDAO> DtosTo(List<ClientDTO> clients);
}