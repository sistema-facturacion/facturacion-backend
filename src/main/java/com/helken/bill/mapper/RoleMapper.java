package com.helken.bill.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import com.helken.bill.models.dao.RoleDAO;
import com.helken.bill.models.dto.RoleDTO;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface RoleMapper {

    RoleDTO toDto(RoleDAO role);

    RoleDAO DtoTo(RoleDTO role);

    List<RoleDTO> toDtos(List<RoleDAO> roles);

    @InheritInverseConfiguration
    List<RoleDAO> DtosTo(List<RoleDTO> roles);
}