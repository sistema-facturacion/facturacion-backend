package com.helken.bill.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import com.helken.bill.models.dao.PermissionDAO;
import com.helken.bill.models.dto.PermissionDTO;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface PermissionMapper {

    PermissionDTO toDto(PermissionDAO Permission);

    PermissionDAO DtoTo(PermissionDTO Permission);

    List<PermissionDTO> toDtos(List<PermissionDAO> Permissions);

    @InheritInverseConfiguration
    List<PermissionDAO> DtosTo(List<PermissionDTO> Permissions);
}