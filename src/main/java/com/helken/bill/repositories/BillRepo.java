package com.helken.bill.repositories;

import org.springframework.data.repository.CrudRepository;

import com.helken.bill.models.dao.BillDAO;

public interface BillRepo extends CrudRepository<BillDAO, Long>{

}
