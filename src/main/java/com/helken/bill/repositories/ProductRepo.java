package com.helken.bill.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.helken.bill.models.dao.ProductDAO;

public interface ProductRepo extends CrudRepository<ProductDAO, Long>{

    /*@Query("select p from product p where p.name like %?1%")
    public List<ProductDAO> findByNombre(String term);*/

    public List<ProductDAO> findByNameContainingIgnoreCase(String term);

    public List<ProductDAO> findByNameStartingWithIgnoreCase(String term);
}
