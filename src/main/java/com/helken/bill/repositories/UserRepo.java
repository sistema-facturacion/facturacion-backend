package com.helken.bill.repositories;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.helken.bill.models.dao.UserDAO;

public interface UserRepo extends JpaRepository<UserDAO, Long>{

    public Optional<UserDAO> findByUsername(String username);    

    @Query("SELECT u FROM UserDAO u WHERE u.email = :username OR u.username = :username AND enabled = true")
    public Optional<UserDAO>  findByEmailOrUsername(@Param("username") String username);

    Optional<UserDAO> findByEmail(String email);
}
