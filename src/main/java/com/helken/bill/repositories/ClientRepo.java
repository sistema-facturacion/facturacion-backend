package com.helken.bill.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.helken.bill.models.dao.ClientDAO;

public interface ClientRepo extends JpaRepository<ClientDAO, Long> {

}
