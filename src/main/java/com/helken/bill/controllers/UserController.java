package com.helken.bill.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.helken.bill.mapper.UserMapper;
import com.helken.bill.models.dto.UserDTO;
import com.helken.bill.services.UserService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

  private final UserService userService;
  private final UserMapper userMapper;

  @GetMapping("/list")
  @PreAuthorize("hasAuthority('READ_USER')")
  public List<UserDTO> index() {
    return userMapper.toDtos(userService.listAll());
  }

  @PostMapping("/create")
  @PreAuthorize("hasAuthority('CREATE_USER')")
  public ResponseEntity<UserDTO> register(@RequestBody UserDTO user) {
    return null;
  }

  @PutMapping("/update")
  @PreAuthorize("hasAuthority('UPDATE_USER')")
  public ResponseEntity<UserDTO> update(@RequestBody UserDTO user) {
    return null;
  }

  @DeleteMapping("/delete")
  @PreAuthorize("hasAuthority('DELETE_USER')")
  public ResponseEntity<UserDTO> delete(@RequestBody UserDTO user) {
    return null;
  }

}
