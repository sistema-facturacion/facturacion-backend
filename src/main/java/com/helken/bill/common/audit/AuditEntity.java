package com.helken.bill.common.audit;

import java.util.Date;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public abstract class AuditEntity {

    @CreatedBy
    @Column(name = "created_by", updatable = false)
    protected Long createdBy;

    @CreatedDate
    @Temporal(TemporalType.DATE)
    @Column(name = "created_date", updatable = false)
    protected Date createdDate;
    
    @LastModifiedBy
    @Column(name = "modified_by")
    protected Long modifiedBy;

    @LastModifiedDate
    @Temporal(TemporalType.DATE)
    @Column(name = "modified_date")
    protected Date modifiedDate;
    
}
