package com.helken.bill.common.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NoResultsFoundException extends RuntimeException {

    public NoResultsFoundException() {
        super("No se han encontrado resultados");
    }

    public NoResultsFoundException(String message) {
        super(message);
    }
}