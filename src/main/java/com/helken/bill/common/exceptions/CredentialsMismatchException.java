package com.helken.bill.common.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class CredentialsMismatchException extends RuntimeException {

    /*public CredentialsMismatchException() {
        super("Las credenciales proporcionadas no coinciden");
    }*/

    public CredentialsMismatchException(String message) {
        super(message);
    }
}