package com.helken.bill.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@RequiredArgsConstructor
public enum EnumDirectory {
    Client("clients"),
    User("users");

    private final String name;
}
