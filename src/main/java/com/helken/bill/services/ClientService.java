package com.helken.bill.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.helken.bill.models.dao.ClientDAO;

public interface ClientService {

    public List<ClientDAO> findAll();

    public Page<ClientDAO> findAll(Pageable pageable);

    public ClientDAO findById(Long id);

    public ClientDAO save(ClientDAO client);

    public ClientDAO update(ClientDAO client);

    public void delete(Long id);

}
