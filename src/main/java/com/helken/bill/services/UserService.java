package com.helken.bill.services;

import java.util.List;

import com.helken.bill.models.dao.UserDAO;

public interface UserService {

    public UserDAO save(UserDAO user);

    public UserDAO findByEmailOrUsername(String username);

    public UserDAO findByUserName(String username);

    public List<UserDAO> listAll();
}
