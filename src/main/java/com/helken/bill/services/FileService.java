package com.helken.bill.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
public interface FileService {

    public Resource upload(String directory, String nameFile) throws MalformedURLException;
    public String copy(String directory, MultipartFile multipartFile) throws IOException;
    public boolean delete(String directory, String nameFile);
    public Path getPath(String directory, String nameFile);
    
}
