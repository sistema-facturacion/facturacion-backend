package com.helken.bill.services.impl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.helken.bill.services.FileService;

@Service
public class FileServiceImpl implements FileService{

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Resource upload(String directory, String nameFile) throws MalformedURLException {
        
        Path pathFile = getPath(directory, nameFile);
        logger.info(pathFile.toString());

        Resource resource = new UrlResource(pathFile.toUri());

        if(!resource.exists() && !resource.isReadable()) {
            pathFile = Paths.get("src/main/resources/static/images").resolve("no-user.png").toAbsolutePath();

            resource = new UrlResource(pathFile.toUri());

            logger.error("Error no se pudo cargar la imagen: " + nameFile);

        }
        return resource;
    }

    @Override
    public String copy(String directory, MultipartFile multipartFile) throws IOException {
        String nameFile = UUID.randomUUID().toString();
        Path rutaArchivo = getPath(directory, nameFile);
        logger.info(rutaArchivo.toString());
        Files.copy(multipartFile.getInputStream(), rutaArchivo);
        return nameFile;
    }

    @Override
    public boolean delete(String directory, String nameFile) {
        if (nameFile != null && nameFile.length() > 0) {
            Path rutaFotoAnterior = Paths.get("uploads").resolve(nameFile).toAbsolutePath();
            File archivoFotoAnterior = rutaFotoAnterior.toFile();
            if (archivoFotoAnterior.exists() && archivoFotoAnterior.canRead()) {
                archivoFotoAnterior.delete();
                return true;
            }
        }
        return false;
    }

    @Override
    public Path getPath(String directory, String nameFile) {
        return Paths.get(directory).resolve(nameFile).toAbsolutePath();
    }
    
}
