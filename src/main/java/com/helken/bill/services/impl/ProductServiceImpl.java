package com.helken.bill.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.helken.bill.models.dao.ProductDAO;
import com.helken.bill.repositories.ProductRepo;
import com.helken.bill.services.ProductService;

public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepo productRepo;

    @Override
    @Transactional(readOnly = true)
    public List<ProductDAO> findProductoByNombre(String term) {
        return productRepo.findByNameContainingIgnoreCase(term);
    }

}
