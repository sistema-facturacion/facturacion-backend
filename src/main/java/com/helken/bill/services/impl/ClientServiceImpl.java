package com.helken.bill.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.helken.bill.models.dao.ClientDAO;
import com.helken.bill.repositories.ClientRepo;
import com.helken.bill.services.ClientService;


@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepo clientRepo;

    @Override
    @Transactional(readOnly = true)
    public List<ClientDAO> findAll() {
        return clientRepo.findAll();
    }

    @Transactional(readOnly = true)
    public Page<ClientDAO> findAll(Pageable pageable) {
        //return clientRepo.findAll(pageable).map(clientMapper::toDto);  
        return clientRepo.findAll(pageable);        
    }

    @Override
    @Transactional(readOnly = true)
    public ClientDAO findById(Long id) {
        return clientRepo.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public ClientDAO save(ClientDAO cliente) {
        return clientRepo.save(cliente);
    }

    @Override
    @Transactional
    public ClientDAO update(ClientDAO cliente) {
        return clientRepo.save(cliente);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        clientRepo.deleteById(id);
    }

}
