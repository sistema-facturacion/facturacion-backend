package com.helken.bill.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.helken.bill.models.dao.UserDAO;
import com.helken.bill.repositories.UserRepo;
import com.helken.bill.services.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepo userRepo;

    @Override
    public UserDAO save(UserDAO user) {
        return userRepo.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDAO findByUserName(String username) {
        return (UserDAO) userRepo.findByUsername(username).get();
    }

    @Override
    public UserDAO findByEmailOrUsername(String username) {
        return userRepo.findByEmailOrUsername(username).orElse(null);
    }

    @Override
    public List<UserDAO> listAll() {
        return userRepo.findAll();
    }

}
