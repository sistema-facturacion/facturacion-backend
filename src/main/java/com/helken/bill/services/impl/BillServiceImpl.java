package com.helken.bill.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.helken.bill.models.dao.BillDAO;
import com.helken.bill.repositories.BillRepo;
import com.helken.bill.services.BillService;

public class BillServiceImpl implements BillService{

    @Autowired
    private BillRepo billRepo;
    
    @Override
    @Transactional(readOnly = true)
    public BillDAO findBillById(Long id) {
        return billRepo.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public BillDAO saveBill(BillDAO factura) {
        return billRepo.save(factura);
    }

    @Override
    @Transactional
    public void deleteBillById(Long id) {
        billRepo.deleteById(id);
    }

}
