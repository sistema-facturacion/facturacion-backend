package com.helken.bill.services;

import java.util.List;

import com.helken.bill.models.dao.ProductDAO;

public interface ProductService {

    public List<ProductDAO> findProductoByNombre(String term);
    
}
