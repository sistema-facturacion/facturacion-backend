package com.helken.bill.services;

import java.io.IOException;

import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.DatabindException;
import com.helken.bill.models.dto.auth.AuthenticationRequest;
import com.helken.bill.models.dto.auth.AuthenticationResponse;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public interface AuthenticationService {

    public AuthenticationResponse authenticate(AuthenticationRequest request, HttpServletResponse response);

    public void refreshToken(HttpServletRequest request, HttpServletResponse response)
            throws IOException, StreamWriteException, DatabindException;
}