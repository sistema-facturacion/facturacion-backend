package com.helken.bill.services;

import com.helken.bill.models.dao.BillDAO;

public interface BillService {

    public BillDAO findBillById(Long id);

    public BillDAO saveBill(BillDAO factura);

    public void deleteBillById(Long id);
    
}
