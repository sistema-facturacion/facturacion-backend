package com.helken.bill.models.dto;

import java.util.Date;

import com.helken.bill.common.audit.AuditEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDTO extends AuditEntity {

    private Long id;
    private String name;
    private Double price;
    private Date createdDate;
}
