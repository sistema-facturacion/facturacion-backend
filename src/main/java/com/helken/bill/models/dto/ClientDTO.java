package com.helken.bill.models.dto;

import java.util.List;

import com.helken.bill.common.audit.AuditEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientDTO extends AuditEntity {
    private Long id;
    private String identity;
    private String names;
    private String lastNames;
    private String email;
    private String picture;
    private RegionDTO region;
    private List<BillDTO> bills;
}
