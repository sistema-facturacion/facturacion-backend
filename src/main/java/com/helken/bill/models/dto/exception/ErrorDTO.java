package com.helken.bill.models.dto.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorDTO {
    private String field;
    private String message;    
}
