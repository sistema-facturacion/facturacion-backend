package com.helken.bill.models.dto;

import java.util.List;

import com.helken.bill.common.audit.AuditEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillDTO extends AuditEntity {

    private Long id;
    private String description;
    private String observation;
    private ClientDTO client;
    private List<BillItemDTO> items;

    public Double getTotal() {
        Double total = 0.00;
        for (BillItemDTO item : items) {
            total += item.getTotal();
        }
        return total;
    }
}
