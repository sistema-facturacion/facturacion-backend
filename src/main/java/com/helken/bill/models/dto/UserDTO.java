package com.helken.bill.models.dto;

import java.io.Serializable;
import java.util.List;

import com.helken.bill.common.audit.AuditEntity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserDTO extends AuditEntity implements Serializable {
    
    private Long id;
    private String identity;
    private String username;
    private String firstName;
    private String lastName;
    //private String password;
    private Boolean enabled;    
    private String email;
    private String picture;
    private List<RoleDTO> roles;
}
