package com.helken.bill.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegionDTO {

    private Long id;
    private String name;
}
