package com.helken.bill.models.dto;

import java.util.List;

import com.helken.bill.models.dao.PermissionDAO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleDTO {

    private Long id;
    private String name;
    private String description;
    private List<PermissionDAO> permissions;
}
