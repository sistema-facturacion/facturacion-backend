package com.helken.bill.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillItemDTO {

    private Long id;
    private Integer quantity;
    private ProductDTO product;

    public Double getTotal() {
        return quantity.doubleValue() * product.getPrice();
    }
}
