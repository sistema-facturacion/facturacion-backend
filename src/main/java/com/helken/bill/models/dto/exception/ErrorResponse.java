package com.helken.bill.models.dto.exception;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorResponse {
    // customizing timestamp serialization format
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date timestamp;

    private int statusCode;

    private String statusName;

    private String message;

    private String stackTrace;

    private Object data;

    public ErrorResponse() {
        timestamp = new Date();
    }

    public ErrorResponse(HttpStatus httpStatus, Exception ex) {
        this();
        this.statusCode = httpStatus.value();
        this.statusName = httpStatus.name();
        this.message = ex.getMessage();
        this.stackTrace = stringWriter(ex);
    }

    public ErrorResponse(HttpStatus httpStatus, Exception ex, Object data) {
        this(httpStatus, ex);
        this.data = data;
    }

    private String stringWriter(Exception e) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        e.printStackTrace(printWriter);
        return stringWriter.toString();
    }
  
}