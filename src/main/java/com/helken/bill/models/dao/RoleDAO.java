package com.helken.bill.models.dao;

import java.io.Serializable;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.UniqueConstraint;

@Getter
@Setter
@Entity
@Table(name = "roles")
public class RoleDAO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //@Enumerated(EnumType.STRING)
    @Column(unique = true, length = 20)
    private String name;
   
    @Column(length = 100)
    private String description;

    /*@ManyToMany(mappedBy="roles")
    private List<UserDAO> users;*/

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "roles_permissions", joinColumns = @JoinColumn(name = "id_role"), inverseJoinColumns = @JoinColumn(name = "id_permission"), uniqueConstraints = {
            @UniqueConstraint(columnNames = { "id_role", "id_permission" }) })
    private List<PermissionDAO> permissions;

}
