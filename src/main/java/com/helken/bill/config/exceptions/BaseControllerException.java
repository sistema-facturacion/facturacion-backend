package com.helken.bill.config.exceptions;

import java.util.HashSet;
import java.util.Set;

import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.helken.bill.common.exceptions.BadGatewayException;
import com.helken.bill.common.exceptions.BadRequestException;
import com.helken.bill.common.exceptions.ConflictException;
import com.helken.bill.common.exceptions.ForbiddenException;
import com.helken.bill.common.exceptions.UnauthorizedException;
import com.helken.bill.models.dto.exception.ErrorDTO;
import com.helken.bill.models.dto.exception.ErrorResponse;

@RestControllerAdvice
public class BaseControllerException {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public ErrorResponse notFoundRequest(Exception ex) {
        return new ErrorResponse(HttpStatus.NOT_FOUND, ex);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({
            BadRequestException.class,
            org.springframework.dao.DuplicateKeyException.class,
            org.springframework.web.bind.support.WebExchangeBindException.class,
            org.springframework.http.converter.HttpMessageNotReadableException.class,
            org.springframework.web.server.ServerWebInputException.class
    })
    public ErrorResponse badRequest(Exception ex) {        
        return new ErrorResponse(HttpStatus.BAD_REQUEST, ex);
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(ConflictException.class)
    public ErrorResponse conflict(Exception ex) {        
        return new ErrorResponse(HttpStatus.CONFLICT, ex);
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(ForbiddenException.class)
    public ErrorResponse forbidden(Exception ex) {        
        return new ErrorResponse(HttpStatus.FORBIDDEN, ex);
    }

    @ResponseStatus(HttpStatus.BAD_GATEWAY)
    @ExceptionHandler(BadGatewayException.class )
    public ErrorResponse badGateway(Exception ex) {        
        return new ErrorResponse(HttpStatus.BAD_GATEWAY, ex);
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({
            UnauthorizedException.class,
            BadCredentialsException.class,
            org.springframework.security.access.AccessDeniedException.class })
    public ErrorResponse handlerValidationException(Exception ex) {
        return new ErrorResponse(HttpStatus.UNAUTHORIZED, ex);
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse handlerValidationException(MethodArgumentNotValidException ex) {
        Set<ErrorDTO> lstError = new HashSet<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            ErrorDTO errorDto = new ErrorDTO();
            String fieldName = ((FieldError) error).getField();
            String message = error.getDefaultMessage();
            errorDto.setField(fieldName);
            errorDto.setMessage(message);
            lstError.add(errorDto);
        });        
        return new ErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY, ex, lstError);
    }    

    //Método que va a permitir interceptar todas las excepciones que no se han interceptado en los metodos anteriores
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorResponse exception(Exception ex) { // The error must be corrected
        System.out.println("entrooooooooooooooooooooooo");
        ex.printStackTrace();        
        return new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex);
    }
    
}
