package com.helken.bill.config;

import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.helken.bill.config.jwt.JwtAuthorizationFilter;
import com.helken.bill.services.UserService;

import lombok.RequiredArgsConstructor;

/*
 * Clase que permite configurar el acceso a cada uno de los endpoints (servicios) * 
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class WebSecurityConfig {

        private final JwtAuthorizationFilter jwtAuthorizationFilter;

        private final UserService userService;

        @Bean
        public SecurityFilterChain securityFilterChain(HttpSecurity http, AuthenticationManager authManager)
                        throws Exception {
                return http.csrf(c -> c.disable())
                                .cors(c -> c.and())
                                .headers(headers -> headers.frameOptions().disable())
                                .authorizeHttpRequests(auth -> auth
                                                .requestMatchers("/auth/authenticate/**").permitAll()
                                                .requestMatchers(PathRequest.toH2Console()).permitAll()
                                                // .requestMatchers("").hasRole("ADMIN")
                                                .anyRequest().authenticated())
                                .sessionManagement(sm -> sm.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                                                .and()
                                                .addFilterBefore(jwtAuthorizationFilter,
                                                                UsernamePasswordAuthenticationFilter.class))
                                .build();
        }

        @Bean
        public UserDetailsService userDetailsService() {
                return username -> userService.findByEmailOrUsername(username);
        }

        @Bean
        public AuthenticationProvider authenticationProvider() {
                //System.out.println("PASS: " + new BCryptPasswordEncoder().encode("admin"));
                DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
                authProvider.setUserDetailsService(userDetailsService());
                authProvider.setPasswordEncoder(passwordEncoder());
                return authProvider;
        }

        @Bean
        public AuthenticationManager authenticationManager(AuthenticationConfiguration authConfig) throws Exception {
                return authConfig.getAuthenticationManager();
        }

        @Bean
        public PasswordEncoder passwordEncoder() {
                return new BCryptPasswordEncoder();
        }
}