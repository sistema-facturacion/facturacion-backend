
/* Creamos algunos usuarios con sus roles */
INSERT INTO `users` (username, identity, password, enabled, first_name, last_name, email) VALUES ('admin', '0123456789', '$2a$10$3nzKPQlJq3m94ig8EUrdTeEqWo4HovFFmVS134LAiJLiYPPB/7HNa',true, 'Fabián', 'Santander','fabian.santander@ucuenca.edu.ec');

INSERT INTO `users` (username, identity, password, enabled, first_name, last_name, email) VALUES ('user', '0123456783', '$2a$10$3nzKPQlJq3m94g8EUrdTeEqWo4HovFFmVS134LAiJLiYPPB/7HNa',true, 'User', 'System','dfabian.santander@ucuenca.edu.ec');



INSERT INTO `roles` (name) VALUES ('ROLE_ADMIN');
INSERT INTO `roles` (name) VALUES ('ROLE_USER');

INSERT INTO `users_roles` (`id_user`, `id_role`) VALUES (1, 1);
INSERT INTO `users_roles` (`id_user`, `id_role`) VALUES (1, 2);

--INSERT INTO `users_roles` (`id_user`, `id_role`) VALUES (2, 2);


INSERT INTO `permissions` (name) VALUES ('CREATE_USER');
INSERT INTO `permissions` (name) VALUES ('DELETE_USER');
INSERT INTO `permissions` (name) VALUES ('UPDATE_USER');
INSERT INTO `permissions` (name) VALUES ('READ_USER');

INSERT INTO `roles_permissions` (`id_role`, `id_permission`) VALUES (1, 1);
INSERT INTO `roles_permissions` (`id_role`, `id_permission`) VALUES (1, 2);
INSERT INTO `roles_permissions` (`id_role`, `id_permission`) VALUES (1, 3);
INSERT INTO `roles_permissions` (`id_role`, `id_permission`) VALUES (1, 4);
INSERT INTO `roles_permissions` (`id_role`, `id_permission`) VALUES (2, 4);



